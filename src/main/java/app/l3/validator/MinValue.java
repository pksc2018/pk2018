package app.l3.validator;

public class MinValue implements java.lang.Comparable<Object> {
    Number value = 5;
    @Override
    public int compareTo(Object obj) {
        Number objNumber = (Number) obj;
        if(value.intValue()==objNumber.intValue()){
            return 0;
        }else if(value.intValue()>objNumber.intValue()){
            return 1;
        }else {
            return -1;
        }
    }
}
