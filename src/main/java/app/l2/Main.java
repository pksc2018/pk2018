package app.l2;

import app.commons.Processor;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
public class Main {

	public static void main(String[] args) {

		Basket basket =  new Basket();
		basket.setCreationDate(new Date());
		for (int i = 0; i < 10; i++) {
			Item item = new Item();
			item.setName("index: " + i);
			item.setPrice(BigDecimal.valueOf(i +1));
			basket.getItems().add(item);
		}

		List<Processor> processors = Lists.newArrayList(new FV22Processor(),new FV23Processor());

		Optional<Processor> first = processors.stream().filter(p -> p.accept(basket)).findFirst();

		if (first.isPresent()){
			log.debug(Objects.toString(first.get().process(basket)));
		}
	}
}
