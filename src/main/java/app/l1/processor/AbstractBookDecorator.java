package app.l1.processor;

import app.l1.model.Article;
import app.l1.model.Book;
import org.mapstruct.factory.Mappers;

public abstract class AbstractBookDecorator implements BookMapper{

	@Override
	public Book convert(Article article) {
		BookMapper mapper = Mappers.getMapper(BookMapper.class);
		Book convert = mapper.convert(article);
		//Do something else
		return convert;
	}
}
