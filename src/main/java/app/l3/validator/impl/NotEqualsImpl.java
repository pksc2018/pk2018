package app.l3.validator.impl;

import app.l3.validator.ContextualValidator;
import app.l3.validator.NotEquals;
import lombok.Setter;

import java.util.Objects;

public class NotEqualsImpl implements ContextualValidator<NotEquals,Object> {
    private Object value;

    @Override
    public void valid(Object obj) {
        if(Objects.equals(value, obj)) {
            throw new IllegalArgumentException("Objects are equal!");
        }
    }

    @Override
    public void processContext(NotEquals value) {
        this.value = value.value();
    }
}
