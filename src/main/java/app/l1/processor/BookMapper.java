package app.l1.processor;

import app.l1.model.Article;
import app.l1.model.Book;
import app.l1.processor.AbstractBookDecorator;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

@Mapper
@DecoratedWith(AbstractBookDecorator.class)
public interface BookMapper {
	Book convert(Article article);
	Article convert(Book book);
}
