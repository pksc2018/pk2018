package app.l2;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Settlement {
	private List<Item> items;
	private BigDecimal gross;
	private VatType vat;
}
