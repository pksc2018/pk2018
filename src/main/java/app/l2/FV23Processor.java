package app.l2;

import app.commons.AbstractProcessor;
import app.commons.Converter;

import java.math.BigDecimal;
import java.util.Date;

public class FV23Processor extends AbstractProcessor<Basket,Settlement> {

	@Override
	protected Converter getConverter() {
		return new SettlementConverter();
	}

	@Override
	protected void doAdditional(Settlement converted) {
		converted.setGross(countGross(converted));
	}

	private BigDecimal countGross(Settlement converted) {
		double sum = converted.getItems().stream().mapToDouble(i -> i.getPrice().doubleValue()).sum();

		return BigDecimal.valueOf(sum * VatType.VAT_23.getVat());
	}

	@Override
	public boolean accept(Basket basket) {
		Date date = new Date("2018/12/30");
		Date creationDate = basket.getCreationDate();
		return creationDate.before(date) || creationDate.equals(date);
	}
}
