package app.l2;

import org.mapstruct.Mapper;

@Mapper
public interface SettlementMapper {
	Settlement convert(Basket basket);
	Basket convert(Settlement settlement);
}
