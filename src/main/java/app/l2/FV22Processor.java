package app.l2;

import app.commons.AbstractProcessor;
import app.commons.Converter;

import java.math.BigDecimal;
import java.util.Date;

public class FV22Processor extends AbstractProcessor<Basket,Settlement> {
	@Override
	protected Converter getConverter() {
		return new SettlementConverter();
	}

	@Override
	protected void doAdditional(Settlement converted) {
		converted.setGross(countGross(converted));
	}

	private BigDecimal countGross(Settlement converted) {
		double sum = converted.getItems().stream().mapToDouble(i -> i.getPrice().doubleValue()).sum();

		return BigDecimal.valueOf(sum * VatType.VAT_23.getVat());
	}

	@Override
	public boolean accept(Basket basket) {
		return basket.getCreationDate().after(new Date("2018/12/30"));
	}
}
