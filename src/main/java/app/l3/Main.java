package app.l3;

import app.l2.Basket;
import app.l2.Item;
import app.l3.validator.ReflectValidator;
import app.l3.validator.ex.ValidatorException;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Slf4j
public class Main {
	public static void main(String[] args) {
		Basket basket = getBasket();
		ReflectShow.print(basket);

		try {
			ReflectValidator.valid(basket);
		} catch (ValidatorException e) {
			log.error("validator return exceptions {}", e.getExceptions());
		}
	}

	private static Basket getBasket() {
		Basket basket = new Basket();
		basket.setCreationDate(new Date());
		basket.setItems(getItems());

		return basket;
	}

	private static List<Item> getItems() {
		List<Item> items = Lists.newArrayList();

		for (int i = 0; i < 10; i++) {
			Item item = new Item();
			item.setName("index: " + i);
			item.setPrice(BigDecimal.valueOf(i + 1));
			items.add(item);
		}

		return items;
	}
}
