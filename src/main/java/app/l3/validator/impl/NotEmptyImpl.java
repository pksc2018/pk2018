package app.l3.validator.impl;

import app.l3.validator.Validator;

import java.util.Objects;

public class NotEmptyImpl implements Validator {
	@Override
	public void valid(Object obj) {
		if (Objects.isNull(obj)) {
			throw new IllegalArgumentException("Field must not be empty");
		}
	}
}
