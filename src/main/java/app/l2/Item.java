package app.l2;

import app.l3.validator.NotEquals;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Item {
	@NotEquals("index: 3")
	private String name;
	//@Min(12)
	private BigDecimal price;
}
