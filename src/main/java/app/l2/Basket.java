package app.l2;

import app.commons.AbstractNameEntity;
import app.l3.validator.NotEmpty;
import com.google.common.collect.Lists;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Basket extends AbstractNameEntity{
	@NotEmpty
	private Date creationDate;
	private List<Item> items = Lists.newArrayList();
}
