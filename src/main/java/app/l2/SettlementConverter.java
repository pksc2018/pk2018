package app.l2;

import app.commons.Converter;
import org.mapstruct.factory.Mappers;

public class SettlementConverter implements Converter<Basket,Settlement> {

	private static SettlementMapper settlementMapper;

	static {
		settlementMapper =  Mappers.getMapper(SettlementMapper.class);
	}

	@Override
	public Settlement convert(Basket basket) {
		return settlementMapper.convert(basket);
	}

	@Override
	public Basket convert(Settlement settlement) {
		return settlementMapper.convert(settlement);
	}
}
