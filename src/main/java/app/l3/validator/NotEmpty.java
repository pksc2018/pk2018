package app.l3.validator;

import app.l3.validator.impl.NotEmptyImpl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(impl = NotEmptyImpl.class)
public @interface NotEmpty {
}
