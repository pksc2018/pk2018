package app.l1.processor;

import app.commons.AbstractProcessor;
import app.commons.Converter;
import app.l1.model.Article;
import app.l1.model.Book;
import org.apache.commons.lang.StringUtils;

public class BookProcessor extends AbstractProcessor<Book, Article> {

	private Converter<Book, Article> converter = new BookArticleConverter();

	@Override
	public Converter<Book, Article> getConverter() {
		return converter;
	}

	@Override
	public boolean accept(Book book) {
		return StringUtils.length(book.getName()) > 10;
	}
}
