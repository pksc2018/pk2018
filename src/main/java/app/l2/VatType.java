package app.l2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum VatType {
	VAT_22(22), VAT_23(23), VAT_8(8);
	int vat;
}
