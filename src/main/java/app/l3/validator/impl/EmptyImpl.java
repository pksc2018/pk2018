package app.l3.validator.impl;

import app.l3.validator.Validator;
import java.util.Objects;

public class EmptyImpl implements Validator {

	@Override
	public void valid(Object obj) {
		if (Objects.nonNull(obj)) {
			throw new IllegalArgumentException("Field must be empty");
		}
	}
}
