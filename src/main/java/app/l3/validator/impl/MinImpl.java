package app.l3.validator.impl;

import app.l3.validator.ContextualValidator;
import app.l3.validator.Min;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.BigInteger;

public class MinImpl implements ContextualValidator<Min, Number> {

    private Integer value;

    @Override
    public void valid(Number obj) {
        if (toBigDecimal(value).compareTo(toBigDecimal(obj)) >=0) {
            throw new IllegalArgumentException("value is too low");
        }
    }

    public static boolean instanceOf(Object obj, Class<?>... cls) {
        for (Class<?> clazz : cls) {
            if (clazz.isInstance(obj)) {
                return true;
            }
        }

        return false;
    }

    private static BigDecimal toBigDecimal(Number number) {
        if(instanceOf(number,BigDecimal.class)) {
            return (BigDecimal) number;
        }
        if(instanceOf(number,BigInteger.class)) {
            return new BigDecimal((BigInteger) number);
        }
        if(instanceOf(number, Byte.class, Short.class, Integer.class, Long.class)) {
            return new BigDecimal(number.longValue());
        }
        if(instanceOf(number, Float.class, Double.class)) {
            return new BigDecimal(number.doubleValue());
        }

        throw new NumberFormatException("Unrecognized number format");
    }

    @Override
    public void processContext(Min value) {
        this.value = value.value();
    }
}
